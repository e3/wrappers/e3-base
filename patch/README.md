# Site-specific EPICS Base Patch Files

The following are the current patch files for EPICS base.

## Creating a new patch file

In order to create a new patch file, make the necessary modifications in the epics-base
submodule, and then run
```
$ git diff --no-prefix path/to/modified/file > ../patch/patch_file_name.p0.patch
```

### Add-support-for-EPICS_DB_INCLUDE_PATH.p0.patch

This updates `dbLoadTemplate` to allow it to search through the paths
provided by the environment variable `EPICS_DB_INCLUDE_PATH` (which is
itself updated by `require` every time we load a module).

This simplifies things so that we do not maintain a custom version of
`dbLoadTemplate` in require.

### os_class.p0.patch

```
diff --git configure/CONFIG_APP_INCLUDE configure/CONFIG_APP_INCLUDE
index 8b4bd7d08..21de4ad39 100644
--- configure/CONFIG_APP_INCLUDE
+++ configure/CONFIG_APP_INCLUDE
@@ -19,7 +19,7 @@ define  RELEASE_FLAGS_template
   $(1)_LIB = $$(wildcard $$(strip $$($(1)))/lib/$(T_A))
   SHRLIB_SEARCH_DIRS += $$($(1)_LIB)
   RELEASE_INCLUDES += $$(addprefix -I,$$(wildcard $$(strip $$($(1)))/include/compiler/$(CMPLR_CLASS)))
-  RELEASE_INCLUDES += $$(addprefix -I,$$(wildcard $$(strip $$($(1)))/include/os/$(OS_CLASS)))
+  RELEASE_INCLUDES += $$(addprefix -I,$$(wildcard $$(strip $$($(1)))/include/os/$$(OS_CLASS)))
   RELEASE_INCLUDES += $$(addprefix -I,$$(wildcard $$(strip $$($(1)))/include))
   RELEASE_DBD_DIRS += $$(wildcard $$(strip $$($(1)))/dbd)
   RELEASE_DB_DIRS += $$(wildcard $$(strip $$($(1)))/db)
```

### enable_new_dtags.p0.patch

```
diff --git configure/CONFIG.gnuCommon configure/CONFIG.gnuCommon
index c4fd8cedd..d30910c5a 100644
--- configure/CONFIG.gnuCommon
+++ configure/CONFIG.gnuCommon
@@ -57,8 +57,8 @@ STATIC_LDFLAGS_YES = -static
 STATIC_LDFLAGS_NO =
 
 SHRLIB_CFLAGS = -fPIC
-SHRLIB_LDFLAGS = -shared -fPIC
-LOADABLE_SHRLIB_LDFLAGS = -shared -fPIC
+SHRLIB_LDFLAGS = -shared -fPIC  -Wl,--enable-new-dtags
+LOADABLE_SHRLIB_LDFLAGS = -shared -fPIC  -Wl,--enable-new-dtags
 
 GNU_LDLIBS_YES = -lgcc
```

### add_registerChannelProviderLocal_to_softIocPVA.p0.patch

The purpose of this patch is to ensure that `pvdbl` is included in `softIocPVA`.

```
Submodule modules/pva2pva contains modified content
diff --git modules/pva2pva/pdbApp/Makefile modules/pva2pva/pdbApp/Makefile
index ee78988..38d0d60 100644
--- modules/pva2pva/pdbApp/Makefile
+++ modules/pva2pva/pdbApp/Makefile
@@ -81,6 +84,7 @@ endif
 softIocPVA_DBD += softIoc.dbd
 softIocPVA_DBD += PVAServerRegister.dbd
 softIocPVA_DBD += qsrv.dbd
+softIocPVA_DBD += registerChannelProviderLocal.dbd
 
 #===========================
```


### remove_mkdir_from_rules_build.p0.patch

This is necessary due to how e3 packages and installs its modules. Without
this change, the library directory is created too early and so `driver.makefile`
detects falsely that the module has already been installed.

```
diff --git configure/RULES_BUILD configure/RULES_BUILD
index 2a78a96b1..b9a43e2c2 100644
--- configure/RULES_BUILD
+++ configure/RULES_BUILD
@@ -307,7 +307,7 @@ $(LOADABLE_SHRLIBNAME): $(LOADABLE_SHRLIB_PREFIX)%$(LOADABLE_SHRLIB_SUFFIX):
 
 $(LIBNAME) $(SHRLIBNAME) $(LOADABLE_SHRLIBNAME): | $(INSTALL_LIB)
 $(INSTALL_LIB):
-       @$(MKDIR) $@
+#      @$(MKDIR) $@
 
 #---------------------------------------------------------------
 # C++ munching for VxWorks
```

### ess_epics_host_arch.p0.patch

This enables two ESS-specific architectures, core-i7-poky, and ppc64e6500.

```
diff --git src/tools/EpicsHostArch.pl src/tools/EpicsHostArch.pl
index e8e49bc5e..3b228035c 100644
--- src/tools/EpicsHostArch.pl
+++ src/tools/EpicsHostArch.pl
@@ -42,6 +42,10 @@ sub HostArch {
         return 'solaris-x86'    if m/^i86pc-solaris/;
 
         my ($kernel, $hostname, $release, $version, $cpu) = uname;
+       if (m/^x86_64-linux/) {
+           if ( $release=~ m/cct$/) {return "linux-corei7-poky";}
+           else                     {return "linux-x86_64"; }
+       }
         if (m/^darwin/) {
             for ($cpu) {
                 return 'darwin-x86' if m/^(i386|x86_64)/;
@@ -49,7 +53,12 @@ sub HostArch {
             }
             die "$0: macOS CPU type '$cpu' not recognized\n";
         }
-
+        if (m/^powerpc64-linux/) {
+            for ($cpu) {
+               return 'linux-ppc64e6500' if m/^ppc64/;
+            }
+            die "$0: linux-ppc64 OS CPU type '$cpu' not recognized\n";
+        }
         die "$0: Architecture '$arch' not recognized\n";
     }
 }
```

### config_site-x86_64_c++11.p0.patch

By agreement at ESS, since EPICS base 7.0.5 we have adopted c++11 as the default c++
standard. This enforces that consistently across all module builds.

### revert_readline_behaviour.p0.patch

In https://github.com/epics-base/epics-base/pull/254, the autodetection of the readline
library was broken for older versions of gcc; in particular, those on Centos7 are affected
by this change. This patch implements the changes from https://github.com/epics-base/epics-base/pull/296
to revert to former behaviour.
