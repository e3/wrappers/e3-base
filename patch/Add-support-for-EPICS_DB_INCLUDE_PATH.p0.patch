From dd7da8283bfd9119e4f007e8f40d2970f77351f3 Mon Sep 17 00:00:00 2001
From: Simon Rose <simon.rose@ess.eu>
Date: Mon, 12 Aug 2024 09:46:54 +0200
Subject: [PATCH] Add support for EPICS_DB_INCLUDE_PATH

This allows dbLoadTemplate to search through either a custom-provided
list of paths, or through paths provided by the environment variable
EPICS_DB_INCLUDE_PATH.
---
 .../database/src/ioc/dbtemplate/dbLoadTemplate.h |  2 +-
 .../database/src/ioc/dbtemplate/dbLoadTemplate.y | 16 ++++++++++++++--
 .../src/ioc/dbtemplate/dbtoolsIocRegister.c      | 15 ++++++++-------
 .../database/test/ioc/dbtemplate/dbltExpand.c    |  2 +-
 4 files changed, 24 insertions(+), 11 deletions(-)

diff --git modules/database/src/ioc/dbtemplate/dbLoadTemplate.h modules/database/src/ioc/dbtemplate/dbLoadTemplate.h
index 71aa9620a..4644564f1 100644
--- modules/database/src/ioc/dbtemplate/dbLoadTemplate.h
+++ modules/database/src/ioc/dbtemplate/dbLoadTemplate.h
@@ -19,7 +19,7 @@ extern "C" {
 #endif
 
 DBCORE_API int dbLoadTemplate(
-    const char *sub_file, const char *cmd_collect);
+    const char *sub_file, const char *cmd_collect, const char *path);
 
 #ifdef __cplusplus
 }
diff --git modules/database/src/ioc/dbtemplate/dbLoadTemplate.y modules/database/src/ioc/dbtemplate/dbLoadTemplate.y
index a382459c1..d68d4660d 100644
--- modules/database/src/ioc/dbtemplate/dbLoadTemplate.y
+++ modules/database/src/ioc/dbtemplate/dbLoadTemplate.y
@@ -12,6 +12,7 @@
 #include <stdlib.h>
 #include <stddef.h>
 #include <string.h>
+#include <errno.h>
 
 #include "osiUnistd.h"
 #include "macLib.h"
@@ -20,7 +21,10 @@
 
 #include "epicsExport.h"
 #include "dbAccess.h"
+#include "dbStaticLib.h"
+#include "dbStaticPvt.h"
 #include "dbLoadTemplate.h"
+#include "osiFileName.h"
 
 static int line_num;
 static int yyerror(char* str);
@@ -324,7 +328,7 @@ static int yyerror(char* str)
 
 static int is_not_inited = 1;
 
-int dbLoadTemplate(const char *sub_file, const char *cmd_collect)
+int dbLoadTemplate(const char *sub_file, const char *cmd_collect, const char *path)
 {
     FILE *fp;
     int i;
@@ -344,8 +348,16 @@ int dbLoadTemplate(const char *sub_file, const char *cmd_collect)
     }
 
     fp = fopen(sub_file, "r");
+    // If the file does not exist locally, and it is not an absolute path...
+    if (!fp && sub_file[0] != OSI_PATH_SEPARATOR[0]) {
+        if (!path || !*path) {
+            path = getenv("EPICS_DB_INCLUDE_PATH");
+        }
+        dbPath(pdbbase, path);
+        dbOpenFile(pdbbase, sub_file, &fp);
+    }
     if (!fp) {
-        fprintf(stderr, "dbLoadTemplate: error opening sub file %s\n", sub_file);
+        fprintf(stderr, "dbLoadTemplate: error opening sub file %s: %s\n", sub_file, strerror(errno));
         return -1;
     }
 
diff --git modules/database/src/ioc/dbtemplate/dbtoolsIocRegister.c modules/database/src/ioc/dbtemplate/dbtoolsIocRegister.c
index f4cfaf5ab..b98dd330b 100644
--- modules/database/src/ioc/dbtemplate/dbtoolsIocRegister.c
+++ modules/database/src/ioc/dbtemplate/dbtoolsIocRegister.c
@@ -15,23 +15,24 @@
 /* dbLoadTemplate */
 static const iocshArg dbLoadTemplateArg0 = {"filename", iocshArgStringPath};
 static const iocshArg dbLoadTemplateArg1 = {"var1=value1,var2=value2", iocshArgString};
-static const iocshArg * const dbLoadTemplateArgs[2] = {
-    &dbLoadTemplateArg0, &dbLoadTemplateArg1
-};
+static const iocshArg dbLoadTemplateArg2 = {"path1:path2:...", iocshArgString};
+static const iocshArg *const dbLoadTemplateArgs[3] = {
+    &dbLoadTemplateArg0, &dbLoadTemplateArg1, &dbLoadTemplateArg2};
 static const iocshFuncDef dbLoadTemplateFuncDef = {
     "dbLoadTemplate",
-    2,
+    3,
     dbLoadTemplateArgs,
     "Load the substitution file given as first argument, apply the substitutions\n"
     "for each template in the substitution file, and load them using 'dbLoadRecords'.\n\n"
     "The second argument provides extra variables to substitute in the\n"
-    "template files (not the substitution file).\n\n"
+    "template files (not the substitution file). The third argument provides\n"
+    "a list of paths to search through for the subsitutions file.\n\n"
     "See 'help dbLoadRecords' for more information.\n\n"
-    "Example: dbLoadTemplate db/my.substitutions 'user=myself,host=myhost'\n",
+    "Example: dbLoadTemplate db/my.substitutions 'user=myself,host=myhost' 'path/to/subst:path2/to2/subst2'\n",
 };
 static void dbLoadTemplateCallFunc(const iocshArgBuf *args)
 {
-    iocshSetError(dbLoadTemplate(args[0].sval, args[1].sval));
+    iocshSetError(dbLoadTemplate(args[0].sval, args[1].sval, args[2].sval));
 }
 
 
diff --git modules/database/test/ioc/dbtemplate/dbltExpand.c modules/database/test/ioc/dbtemplate/dbltExpand.c
index ab44c67b2..84dbefd41 100644
--- modules/database/test/ioc/dbtemplate/dbltExpand.c
+++ modules/database/test/ioc/dbtemplate/dbltExpand.c
@@ -93,7 +93,7 @@ int main(int argc, char **argv)
         exit(1);
     }
 
-    dbLoadTemplate(argv[1], NULL);
+    dbLoadTemplate(argv[1], NULL, NULL);
 
     free(output_buffer);
     free(input_buffer);
-- 
2.39.2 (Apple Git-143)

